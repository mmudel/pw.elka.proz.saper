package pw.eiti.saper.view;
import javax.swing.*;
import javax.swing.Timer;
import pw.eiti.saper.controller.*;
import java.awt.event.*;
import java.awt.*;

public class Gui {
	private Controller controller;
	private JButton buttons[][];
	private JFrame frame;
	private JPanel board;
	private JPanel informationFields;
	private JPanel startGamePanel;
	private JLabel bombs;
	private JLabel time;
	private JButton newGameButton;
	private Timer timer = new Timer(1000, new TimerTime());
	public Gui(TypesOfGame typeOfGame, Controller controller1)
	{
		this.controller=controller1;
		frame = new JFrame("Saper");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		buildMenuBar(typeOfGame);
		buildFrame(typeOfGame);
		frame.setVisible(true);
	    timer.start();
	}
	public void newGame(TypesOfGame typeOfGame)
	{
		buildFrame(typeOfGame);
		((JComponent)frame.getContentPane()).updateUI();
		timer.start();
	}
	private void buildFrame(TypesOfGame type)
	{
		int size = 0;
		switch(type)
		{
		case EASY:
			size=9;
			break;
		case MEDIUM:
			size=16;
			break;
		case HARD:
			size=22;
		}
		frame.setSize(new Dimension(size*53, 600));
		board = new JPanel();
        board.setLayout(new GridLayout(size, size));
        board.setBorder(BorderFactory.createEmptyBorder(10, 30, 20, 30));
        informationFields = new JPanel();
        informationFields.setLayout(new GridLayout(1,7));
        informationFields.setBorder(BorderFactory.createEmptyBorder(0,20,5,20));
        startGamePanel = new JPanel();
        FlowLayout layout = new FlowLayout();
        layout.setAlignment(FlowLayout.CENTER);
        startGamePanel.setLayout(layout);
        startGamePanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        newGameButton = new JButton("");
        newGameButton.setIcon(new ImageIcon(getClass().getResource("thumbMiddle.gif")));
        newGameButton.addActionListener(newGame);
        startGamePanel.add(newGameButton);
		frame.getContentPane().removeAll();
		buttons = new JButton[size][size];
			for(int i=0; i<buttons.length; i++)
			{
			    for(int j=0; j<buttons[i].length; j++)
			    {
			        buttons[i][j] = new JButton();
			        buttons[i][j].addMouseListener(new ButtonListener());
			        board.add(buttons[i][j]);
			    }
			}
		ImageIcon bombIcon = new ImageIcon(getClass().getResource("mina.gif"));
		ImageIcon clockIcon = new ImageIcon(getClass().getResource("zegar.gif"));
		JLabel clockIconLabel = new JLabel();
		clockIconLabel.setIcon(clockIcon);
		JLabel bombIconLabel = new JLabel();
		bombIconLabel.setIcon(bombIcon);
		time = new JLabel("0");
		bombs = new JLabel(Integer.toString(controller.howManyBombsLeft()));
		informationFields.add(clockIconLabel);
		informationFields.add(time);
		informationFields.add(new Label(""));
		informationFields.add(new Label(""));
		informationFields.add(new Label(""));
	    informationFields.add(bombs);
	    informationFields.add(bombIconLabel);
		frame.getContentPane().add(board, BorderLayout.CENTER);
		frame.getContentPane().add(informationFields, BorderLayout.SOUTH);
		frame.getContentPane().add(startGamePanel, BorderLayout.NORTH);
	}
	private void buildMenuBar(TypesOfGame type)
	{
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Game");
		JMenuItem menuItem = new JMenuItem("New game", KeyEvent.VK_N);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(
		        "F2"));
		menuItem.addActionListener(newGame);
		menu.add(menuItem);
		menu.addSeparator();
		menuItem = new JMenuItem("Statistics", KeyEvent.VK_S);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(
		        "F3"));
		menuItem.addActionListener(highscore);
		menu.add(menuItem);
		JMenu submenu = new JMenu("Difficulty");
		submenu.setMnemonic(KeyEvent.VK_D);
		ButtonGroup group = new ButtonGroup();
		JRadioButtonMenuItem rbMenuItem = new JRadioButtonMenuItem("Easy");
		if(type==TypesOfGame.EASY)
			rbMenuItem.setSelected(true);
		else
			rbMenuItem.setSelected(false);
		rbMenuItem.setMnemonic(KeyEvent.VK_E);
		rbMenuItem.addActionListener(changeMode);
		group.add(rbMenuItem);
		submenu.add(rbMenuItem);
		rbMenuItem = new JRadioButtonMenuItem("Medium");
		if(type==TypesOfGame.MEDIUM)
			rbMenuItem.setSelected(true);
		else
			rbMenuItem.setSelected(false);;
		rbMenuItem.setMnemonic(KeyEvent.VK_M);
		rbMenuItem.addActionListener(changeMode);
		group.add(rbMenuItem);
		submenu.add(rbMenuItem);
		rbMenuItem = new JRadioButtonMenuItem("Hard");
		if(type==TypesOfGame.HARD)
			rbMenuItem.setSelected(true);
		else
			rbMenuItem.setSelected(false);
		rbMenuItem.setMnemonic(KeyEvent.VK_H);
		rbMenuItem.addActionListener(changeMode);
		group.add(rbMenuItem);
		submenu.add(rbMenuItem);
		menu.add(submenu);
		menuBar.add(menu);
		frame.setJMenuBar(menuBar);
	}
	public void click(int x, int y, int number)
	{
		buttons[x][y].setIcon(null);
		buttons[x][y].setText(Integer.toString(number));
		buttons[x][y].getModel().setPressed(true);
		buttons[x][y].getModel().setEnabled(false);
	}
	public void clickEmptyField(int x, int y)
	{
		buttons[x][y].setIcon(null);
		buttons[x][y].getModel().setPressed(true);
		buttons[x][y].getModel().setEnabled(false);
	}
	public void setFlag(int x, int y, Flag flag)
	{
		ImageIcon bombFieldIcon = new ImageIcon(getClass().getResource("Minefield1.gif"));
		ImageIcon askMark = new ImageIcon(getClass().getResource("znak-zapytania.gif"));
		switch(flag)
		{
		case NO_FLAG:
			buttons[x][y].setIcon(null);
			break;
		case BOMB_FIELD:
			buttons[x][y].setIcon(bombFieldIcon);
			break;
		case NOT_SURE_FIELD:
			buttons[x][y].setIcon(askMark);
		}
	}
	public void failEndGame(int x, int y)
	{
		newGameButton.setIcon(new ImageIcon(getClass().getResource("thumbDown.gif")));
		ImageIcon bombIcon = new ImageIcon(getClass().getResource("mina.gif"));
		buttons[x][y].setEnabled(true);
		buttons[x][y].setBackground(Color.RED);
		buttons[x][y].setIcon(bombIcon);
		controller.showBombs();
		controller.endGame();
		JOptionPane.showMessageDialog(frame, "You lost!");
		return;
	}
	public void successEndGame()
	{
		newGameButton.setIcon(new ImageIcon(getClass().getResource("thumbUp.gif")));
		controller.endGame();
		JOptionPane.showMessageDialog(frame, "You won!");
		return;
	}
	public int getTime()
	{
		return Integer.parseInt(time.getText());
	}
	public void timerStop()
	{
		timer.stop();
	}
	public void showBomb(int x, int y)
	{
		ImageIcon bombIcon = new ImageIcon(getClass().getResource("mina.gif"));
		buttons[x][y].setIcon(bombIcon);
	}
	public void showNoBomb(int x, int y)
	{
		buttons[x][y].setIcon(new ImageIcon(getClass().getResource("noBomb.gif")));
	}
	public JButton[][] getButtons()
	{
		return buttons;
	}
	private ActionListener highscore = new ActionListener(){
		public void actionPerformed(ActionEvent evt)
		{
			new ShowHighscore();
		}
	};
	private ActionListener newGame = new ActionListener(){
		public void actionPerformed(ActionEvent evt)
		{
			controller.newGame();
		}
	};
	private ActionListener changeMode = new ActionListener() {
		public void actionPerformed(ActionEvent evt)
		{
			switch(((JRadioButtonMenuItem)evt.getSource()).getText())
			{
			case "Easy":
				controller.setTypeOfGame(TypesOfGame.EASY);
				break;
			case "Medium":
				controller.setTypeOfGame(TypesOfGame.MEDIUM);
				break;
			case "Hard":
				controller.setTypeOfGame(TypesOfGame.HARD);
			}
		}
	};
	private class TimerTime implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
	    {
	        time.setText(Long.toString(controller.getTime()));
	    }
	}
	private class ButtonListener implements MouseListener{
		boolean pressed;
		public void mouseClicked(MouseEvent evt)
		{
		}
		public void mousePressed(MouseEvent evt)
		{
			if(SwingUtilities.isLeftMouseButton(evt))
				((JButton)evt.getSource()).getModel().setPressed(true);
			pressed=true;
		}
		public void mouseReleased(MouseEvent evt)
		{
			if(pressed)
			{
				pressed = false;
				JButton button = ((JButton)evt.getSource());
				int i=0, j=0;
				for(i=0; i<buttons.length; i++)
				{
				    for(j=0; j<buttons[i].length; j++)
				    {
				    	if (button==buttons[i][j])
				    	{
				    		if(SwingUtilities.isRightMouseButton(evt))
				    		{
				    			controller.rightClick(i, j);
				    			bombs.setText(Integer.toString(controller.howManyBombsLeft()));
				    		}
				    		else if(SwingUtilities.isMiddleMouseButton(evt))
				    			return;
				    		else
				    		{
				    			controller.checkField(i, j);
				    		}
				    		return;
				    	}
				    }
				}
			}
		}
		public void mouseEntered(MouseEvent evt)
		{
			pressed = true;
		}
		public void mouseExited(MouseEvent evt)
		{
			pressed = false;
		}
	}
}
