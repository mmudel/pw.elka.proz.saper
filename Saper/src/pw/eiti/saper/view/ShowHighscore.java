package pw.eiti.saper.view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.*;
import java.util.Scanner;

import javax.swing.*;

public class ShowHighscore {
	ShowHighscore()
	{
		Scanner read=null;
		try{
		File file = new File("highscore.txt");
		read = new Scanner(file);
		}
		catch(FileNotFoundException exc)
		{
			System.out.println("NO SUCH FILE");
		}
		JFrame highscoreFrame = new JFrame("Highscore");
		highscoreFrame.setResizable(false);
		highscoreFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		highscoreFrame.setSize(new Dimension(200, 100));
		JComponent pane = (JComponent)highscoreFrame.getContentPane();
		pane.setLayout(new GridLayout(3, 2));
		pane.add(new JLabel("Easy score:"));
		pane.add(new JLabel(read.nextLine()));
		pane.add(new JLabel("Medium score:"));
		pane.add(new JLabel(read.nextLine()));
		pane.add(new JLabel("Hard score:"));
		pane.add(new JLabel(read.nextLine()));
		read.close();
		highscoreFrame.setVisible(true);
	}
}
