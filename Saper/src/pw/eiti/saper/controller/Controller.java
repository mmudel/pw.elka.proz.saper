package pw.eiti.saper.controller;

import pw.eiti.saper.view.*;
import pw.eiti.saper.model.*;
import java.io.File;
import java.util.*;


public class Controller{
	private Board board;
	private TypesOfGame type;
	private Gui gui;
	private int fieldsLeft;
	private long timeStart=0;
	private boolean firstClick = false;
	private boolean gameEnded = false;

	public Controller(Board board)
	{
		File file = new File("highscore.txt");
		if(file.exists()==false)
			new SaveHighscore().createFile();
		this.type = board.getTypeOfGame();
		this.board=board;
		fieldsLeft=(board.getSize()*board.getSize()-board.getBombLeft());
	}
	public void addGui(Gui gui)
	{
		this.gui=gui;
	}
	public void checkField(int x, int y)
	{
		if(gameEnded==true)
			return;
		if(firstClick==false)
			startTime();
		switch(board.checkField(x,y))
		{
		case CLICKED:
			return;
		case BOMB_FIELD:
			return;
		case BOMB:
			gui.failEndGame(x, y);
			break;
		case NO_BOMB:
			board.click(x,y);
			break;
		}
	}
	public void click(int x, int y, int number)
	{
		if(number==0)
			gui.clickEmptyField(x, y);
		else
			gui.click(x, y, number);
		fieldsLeft--;
		if(fieldsLeft==0)
			gui.successEndGame();
	}
	public void rightClick(int x, int y)
	{
		if(board.checkField(x, y)!=fieldState.CLICKED && gameEnded == false)
			board.setFlag(x, y);
	}
	public void flagSetted(int x, int y, Flag flag)
	{
		gui.setFlag(x, y, flag);
	}
	public void setTypeOfGame(TypesOfGame type)
	{
		board.setTypeOfGame(type);
	}
	public void newGame()
	{
		gameEnded=false;
		firstClick=false;
		timeStart=0;
		board.newGame();
		this.type=board.getTypeOfGame();
		fieldsLeft=(board.getSize()*board.getSize()-board.getBombLeft());
		gui.newGame(board.getTypeOfGame());
	}
	public int howManyBombsLeft()
	{
		return board.getBombLeft();
	}
	public long getTime() {
		if(timeStart==0)
			return 0L;
		else
			return (new Date().getTime() - timeStart)/1000;
	}
	private void startTime()
	{
		timeStart = new Date().getTime();
		firstClick=true;
	}
	public void endGame()
	{
		if(fieldsLeft==0)
			new SaveHighscore().saveToFile(gui.getTime(), type);
		gui.timerStop();
		gameEnded = true;
	}
	public void showBombs()
	{
		for(int i=0; i<gui.getButtons().length; i++)
		{
		    for(int j=0; j<gui.getButtons()[i].length; j++)
		    {
		    	if((board.isFieldBombed(i, j)) == true)
		    	{
		    		gui.showBomb(i, j);
		    	}
		    	else if((board.isFieldBombed(i, j)) == false && board.getFieldFlag(i, j) == Flag.BOMB_FIELD)
		    	{
		    		gui.showNoBomb(i, j);
		    	}
		    }
		}
	}
}

