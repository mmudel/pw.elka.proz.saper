package pw.eiti.saper.controller;

import java.io.*;
import java.util.Scanner;

public class SaveHighscore {
	public void createFile()
	{
		PrintWriter file = null;
		try{
		file = new PrintWriter("highscore.txt");
		file.println("brak");
		file.println("brak");
		file.println("brak");
		file.close();
		}
		catch(FileNotFoundException exc)
		{
			System.out.println("ERROR");
		}
	}
	public void saveToFile(int highscore, TypesOfGame type)
	{
		String easyScore, mediumScore, hardScore;
		try{
			File file = new File("highscore.txt");
			PrintWriter save;
			Scanner read = new Scanner(file);
			easyScore = read.nextLine();
			mediumScore = read.nextLine();
			hardScore = read.nextLine();
			save = new PrintWriter(new FileWriter(file));
			switch(type)
			{
			case EASY:
				if(easyScore.equals("brak") || highscore < Integer.parseInt(easyScore) )
					save.println(Integer.toString(highscore));
				else
					save.println(easyScore);
				save.println(mediumScore);
				save.println(hardScore);
				break;
			case MEDIUM:
				save.println(easyScore);
				if(mediumScore.equals("brak") || highscore < Integer.parseInt(mediumScore))
					save.println(Integer.toString(highscore));
				else
					save.println(mediumScore);
				save.println(hardScore);
				break;
			case HARD:
				save.println(easyScore);
				save.println(mediumScore);
				if(hardScore.equals("brak") || highscore < Integer.parseInt(hardScore))
					save.println(Integer.toString(highscore));
				else
					save.println(hardScore);
				break;
			}
			read.close();
			save.close();
			}
			catch(FileNotFoundException exc)
			{
				System.out.println("ERROR");
			}
			catch(IOException exc)
			{
				System.out.println("ERROR");
			}
	}
}
