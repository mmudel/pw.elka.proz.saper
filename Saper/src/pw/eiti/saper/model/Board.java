package pw.eiti.saper.model;

import java.util.*;

import pw.eiti.saper.controller.*;

public class Board {
	private Field[][] fields;
	private TypesOfGame typeOfGame;
	private int bombLeft;
	private int size;
	private Controller controller;

	public Board(TypesOfGame type)
	{
		this.typeOfGame=type;
		newGame();
	}
	public void newGame()
	{
			switch(typeOfGame)
			{
			case EASY:
				createBoard(9, 10);
				this.size=9;
				break;
			case MEDIUM:
				createBoard(16, 40);
				this.size=16;
				break;
			case HARD:
				createBoard(22, 90);
				this.size=22;
		}
	}
	void createBoard(int size, int bombs)
	{
		bombLeft = bombs;
		fields = new Field[size][size];
		for(int i=0; i< fields.length; i++)
		    for(int j=0; j< fields[i].length; j++)
		        fields[i][j] = new Field();
		randomBoard(size, bombs);
	}
	void randomBoard(int size, int bombs)
	{
		Random rand = new Random();
		for(int i=1; i<=bombs; i++)
		{
			int a, b;
			while(true)
			{
				a = rand.nextInt(size);
				b = rand.nextInt(size);
				if(!(fields[a][b].isBomb()))
				{
					fields[a][b].setBomb(true);
					break;
				}
			}
		}
		for(int i=0; i< fields.length; i++)
		{
		    for(int j=0; j< fields[i].length; j++)
		    {
		    	int howManyBombs=0;
		    	if(!(fields[i][j].isBomb()))
		    	{
		    		if(i-1 >= 0 && j-1 >= 0 && fields[i-1][j-1].isBomb())
		    			howManyBombs++;
		    		if(i-1>=0 && fields[i-1][j].isBomb())
		    			howManyBombs++;
		    		if(i-1>=0 && j+1 < size && fields[i-1][j+1].isBomb())
		    			howManyBombs++;
		    		if(j-1>=0 && fields[i][j-1].isBomb())
		    			howManyBombs++;
		    		if(j+1<size && fields[i][j+1].isBomb())
		    			howManyBombs++;
		    		if(i+1 < size && j+1 < size && fields[i+1][j+1].isBomb())
		    			howManyBombs++;
		    		if(i+1 < size && fields[i+1][j].isBomb())
		    			howManyBombs++;
		    		if(i+1 < size && j-1>=0 && fields[i+1][j-1].isBomb())
		    			howManyBombs++;
		    		fields[i][j].setNumber(howManyBombs);
		    	}
		    }
		}
	}
	public TypesOfGame getTypeOfGame() {
		return typeOfGame;
	}
	public void addController(Controller controller)
	{
		this.controller=controller;
	}
	public void setTypeOfGame(TypesOfGame typeOfGame) {
		this.typeOfGame = typeOfGame;
	}
	public void setFlag(int x, int y)
	{
		if(fields[x][y].getFlag()==Flag.BOMB_FIELD)
		{
			bombLeft++;
			fields[x][y].setFlag(Flag.NOT_SURE_FIELD);
			controller.flagSetted(x, y, Flag.NOT_SURE_FIELD);
		}
		else if(fields[x][y].getFlag()==Flag.NOT_SURE_FIELD)
		{
			fields[x][y].setFlag(Flag.NO_FLAG);
			controller.flagSetted(x, y, Flag.NO_FLAG);
		}
		else
		{
			bombLeft--;
			fields[x][y].setFlag(Flag.BOMB_FIELD);
			controller.flagSetted(x, y, Flag.BOMB_FIELD);
		}
	}
	public fieldState checkField(int x, int y)
	{
		if(fields[x][y].isClicked())
			return fieldState.CLICKED;
		else if(fields[x][y].getFlag()==Flag.BOMB_FIELD)
			return fieldState.BOMB_FIELD;
		else if(fields[x][y].isBomb())
			return fieldState.BOMB;
		else
			return fieldState.NO_BOMB;
	}
	public int getNumber(int x, int y)
	{
		return fields[x][y].getNumber();
	}
	public fieldState click(int x, int y)
	{
		if(!(fields[x][y].isClicked()) && fields[x][y].getFlag()!=Flag.BOMB_FIELD && !(fields[x][y].isBomb()))
		{
			fields[x][y].setClicked(true);
			controller.click(x, y, getNumber(x, y));
			if(fields[x][y].getNumber()==0)
			{
	    		if(x-1 >= 0 && y-1 >= 0)
	    			click(x-1,y-1);
	    		if(x-1>=0)
	    			click(x-1,y);
	    		if(x-1>=0 && y+1 < size)
	    			click(x-1,y+1);
	    		if(y-1>=0)
	    			click(x,y-1);
	    		if(y+1<size)
	    			click(x,y+1);
	    		if(x+1 < size && y+1 < size)
	    			click(x+1, y+1);
	    		if(x+1 < size)
	    			click(x+1,y);
	    		if(x+1 < size && y-1>=0)
	    			click(x+1,y-1);
			}
		}
    	return checkField(x, y);
	}
	public int getBombLeft() {
		return bombLeft;
	}
	public void setBombLeft(int bombLeft) {
		this.bombLeft = bombLeft;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public boolean isFieldBombed(int x, int y)
	{
		return fields[x][y].isBomb();
	}
	public Flag getFieldFlag(int x, int y)
	{
		return fields[x][y].getFlag();
	}

}
