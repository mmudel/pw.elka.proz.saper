package pw.eiti.saper.model;

import pw.eiti.saper.controller.Flag;

public class Field {
	boolean bomb = false;
	boolean clicked = false;
	Flag flag = Flag.NO_FLAG; 
	int number;
	

	public Flag getFlag() {
		return flag;
	}

	public void setFlag(Flag flag) {
		this.flag = flag;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public boolean isBomb() {
		return bomb;
	}

	public void setBomb(boolean bomb) {
		this.bomb = bomb;
	}

	public boolean isClicked() {
		return clicked;
	}

	public void setClicked(boolean clicked) {
		this.clicked = clicked;
	}
}