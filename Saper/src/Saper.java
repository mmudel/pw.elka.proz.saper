import pw.eiti.saper.controller.*;
import pw.eiti.saper.model.*;
import pw.eiti.saper.view.*;

public class Saper {

	public static void main(String[] args) {
		Board board = new Board(TypesOfGame.MEDIUM);
		Controller controller = new Controller(board);
		Gui gui = new Gui(TypesOfGame.MEDIUM, controller);
		controller.addGui(gui);
		board.addController(controller);
	}
	
	public static void printBoard(Board d)
	{
		int ileBomb=0;
		for(int j=0; j < d.getSize(); j++)
		{
		    for(int i=0; i< d.getSize(); i++)
		    {
		        if(d.checkField(i, j)==fieldState.BOMB)
		        {
		        	ileBomb++;
		        	System.out.print("B ");
		        }
		        else if(d.checkField(i, j) == fieldState.NO_BOMB)
		        {
		        	System.out.print(d.getNumber(i, j) + " ");
		        }
		        else
		        {
		        	System.out.print("BF ");
		        }
		        if(d.checkField(i, j)==fieldState.CLICKED)
		        	System.out.print("clicked ");
		    }
		    System.out.println();
		}
		System.out.println(ileBomb);
	}

}
